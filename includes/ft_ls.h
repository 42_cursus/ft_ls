/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 14:44:07 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 22:19:22 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# define ILLEGAL_ERROR "ft_ls: illegal option -- "
# define USAGE_ERROR "usage: ./ft_ls [-GRafglrtu] [file ...]"
# define MAJOR(x)((int32_t)(((u_int32_t)(x) >> 24) & 0xff))
# define MINOR(x)((int32_t)((x) & 0xffffff))
# define SIX_MONTH_SECOND 15778463
# define ENABLED_OPTS "1GRafglrtu"

# include "libft.h"
# include <dirent.h>
# include <time.h>
# include <pwd.h>
# include <grp.h>
# include <errno.h>
# include <sys/xattr.h>
# include <sys/acl.h>
# include <sys/stat.h>
# include <uuid/uuid.h>

typedef struct	s_form
{
	size_t	total;
	size_t	size;
	size_t	group;
	size_t	user;
	size_t	link;
	size_t	minor;
	size_t	major;
}				t_form;

typedef struct	s_item
{
	char	*name;
	char	*path;
	mode_t	st_mode;
	nlink_t	st_nlink;
	uid_t	st_uid;
	gid_t	st_gid;
	off_t	st_size;
	quad_t	st_blocks;
	dev_t	st_rdev;
	time_t	date;
}				t_item;

typedef struct	s_ls
{
	char	*opts;
	t_list	*path;
	t_list	*path_files;
	t_list	*path_dir;
	t_bool	optset_before;
	t_bool	first_dir_written;
	t_bool	multidir;
	t_bool	mor_written;
}				t_ls;

t_ls			*ls_init(void);
void			check_arg(t_ls *ls, int argc, char **argv);

void			ls_directories_more(t_ls *ls, t_list *directories);
void			ls_directories(t_ls *ls);
void			ls_files(t_ls *ls);
void			ls_builder(t_ls *ls);
void			ls_kernel(t_ls *ls);

t_item			*item_init(t_ls *ls, char *name, char *path);
t_list			*item_group_init(t_ls *ls, t_list *item_path);
t_item			*item_get(t_ls *ls, struct dirent *file, char *path);
void			item_free(void *item, size_t size);
void			item_lst_free(t_list **lst);

t_list			*items_sort(t_ls *ls, t_list *items);
t_list			*sort(t_list *list, int (*sort_cmp)(t_item *i1, t_item *i2));
void			items_reverse_sort(t_list **list);

void			print_number(int number, int space);
void			print_number_st_size(t_form *form, int number);
void			print_string(char *str, int space);
void			print_string_colored(char *str, mode_t mode, t_bool g_enabled);
void			print_convert_time_t(time_t date);

void			print_type(t_item *item);
void			print_access(t_item *item);
void			print_acl(t_item *item);
void			print_major_minor(t_item *item, t_form *form);
void			print_ln(t_item *item, t_bool link);

t_bool			ls_write_total(t_ls *ls, t_list *items);
void			ls_write_long(t_ls *ls, t_list *items, t_bool total);
void			ls_write_recursion(t_ls *ls, t_list *items);
void			ls_write_simple(t_ls *ls, t_list *items);
void			ls_write(t_ls *ls, t_list *items, t_bool total);

void			ls_write_long_more(t_ls *ls, t_item *cur, t_form *form);
void			ls_write_recursion_more(t_ls *ls, char *path);

void			form_get_correct(t_form *form, t_item *cur);
void			form_get(t_form *form, t_item *cur);
t_form			*form_init(t_ls *ls, t_list *items);

t_bool			is_opt(char opt);
t_bool			enabled_opt(t_ls *ls, char opt);
void			add_opt(t_ls *ls, char opt);
void			check_opt(t_ls *ls, char *opt);

void			check_path(t_ls *ls, char *path);
t_bool			cmp_date(t_item *item1, t_item *item2);
t_bool			cmp_ascii(t_item *item1, t_item *item2);
void			get_color(mode_t mode);
char			*get_absolute_path(char *path);

void			malloc_error(char *code);
void			illegal_usage_error(char opt);
void			print_error(char *name, char *error, t_bool ext);

#endif
