/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_write.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/07 17:03:06 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 11:08:28 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ls_write_long(t_ls *ls, t_list *items, t_bool total)
{
	t_item	*tmp;
	t_form	*form;

	form = form_init(ls, items);
	if (total && ls_write_total(ls, items))
	{
		ft_putstr("total ");
		ft_putnbr(form->total);
		ft_putchar('\n');
	}
	while (items)
	{
		tmp = (t_item*)items->content;
		if (!(enabled_opt(ls, 'a') == FALSE && tmp->name[0] == '.'))
			ls_write_long_more(ls, tmp, form);
		items = items->next;
	}
	ft_memdel((void**)&form);
}

void	ls_write_recursion(t_ls *ls, t_list *items)
{
	t_item	*tmp;

	while (items)
	{
		tmp = (t_item*)items->content;
		if (tmp->name && tmp->path && S_ISDIR(tmp->st_mode))
			if (ft_strcmp(".", tmp->name) && ft_strcmp("..", tmp->name))
				if (!(!enabled_opt(ls, 'a') && tmp->name[0] == '.'))
					ls_write_recursion_more(ls, tmp->path);
		items = items->next;
	}
}

void	ls_write_simple(t_ls *ls, t_list *items)
{
	t_item	*tmp;

	while (items)
	{
		tmp = (t_item*)items->content;
		if (!(enabled_opt(ls, 'a') == FALSE && tmp->name[0] == '.'))
		{
			print_string_colored(tmp->name, tmp->st_mode, enabled_opt(ls, 'G'));
			ft_putchar('\n');
		}
		items = items->next;
	}
}

void	ls_write(t_ls *ls, t_list *items, t_bool total)
{
	items = items_sort(ls, items);
	if (enabled_opt(ls, 'l') || enabled_opt(ls, 'g'))
		ls_write_long(ls, items, total);
	else
		ls_write_simple(ls, items);
	if (enabled_opt(ls, 'R'))
		ls_write_recursion(ls, items);
}

t_bool	ls_write_total(t_ls *ls, t_list *items)
{
	while (items)
	{
		if (((t_item*)items->content)->name[0] != '.')
			return (TRUE);
		items = items->next;
	}
	return (enabled_opt(ls, 'a')) ? TRUE : FALSE;
}
