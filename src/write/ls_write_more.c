/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_write_more.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/08 11:03:38 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 22:21:57 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ls_write_recursion_more(t_ls *ls, char *path)
{
	DIR		*dir;
	t_list	*dir_content;
	t_item	*tmp;

	dir_content = NULL;
	ft_putchar('\n');
	ft_putstr(path);
	ft_putstr(":\n");
	if ((dir = opendir(path)) != NULL)
	{
		while ((tmp = item_get(ls, readdir(dir), path)))
			ft_lstpushback(&dir_content, ft_lstnew(tmp, sizeof(t_item)));
		closedir(dir);
		if (dir_content)
		{
			ls_write(ls, dir_content, TRUE);
			item_lst_free(&dir_content);
		}
	}
	else
		print_error("ft_ls: ", path, FALSE);
}

void	ls_write_long_more(t_ls *ls, t_item *cur, t_form *form)
{
	print_type(cur);
	print_number(cur->st_nlink, form->link);
	if (!enabled_opt(ls, 'g'))
	{
		if (getpwuid(cur->st_uid))
			print_string(getpwuid(cur->st_uid)->pw_name, form->user);
		else
			print_string(ft_itoa(cur->st_uid), form->user);
	}
	if (getgrgid(cur->st_gid))
		print_string(getgrgid(cur->st_gid)->gr_name, form->group);
	else
		print_string(ft_itoa(cur->st_gid), form->group);
	if (S_ISCHR(cur->st_mode) || S_ISBLK(cur->st_mode))
	{
		print_major_minor(cur, form);
		ls->mor_written = TRUE;
	}
	else
		(ls->mor_written) ? print_number_st_size(form, cur->st_size)
		: print_number(cur->st_size, form->size);
	print_convert_time_t(cur->date);
	print_string_colored(cur->name, cur->st_mode, enabled_opt(ls, 'G'));
	print_ln(cur, S_ISLNK(cur->st_mode));
	ft_putchar('\n');
}
