/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_kernel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 13:58:27 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 15:34:09 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ls_directories_more(t_ls *ls, t_list *directories)
{
	DIR		*dir;
	t_list	*dir_content;
	t_item	*cur;
	t_item	*tmp;

	dir_content = NULL;
	while (directories)
	{
		cur = (t_item*)(directories->content);
		dir = opendir(cur->name);
		while ((tmp = item_get(ls, readdir(dir), cur->path)))
			ft_lstpushback(&dir_content, ft_lstnew(tmp, sizeof(t_item)));
		closedir(dir);
		if (dir_content)
		{
			(ls->first_dir_written == TRUE) ? ft_putchar('\n') : NULL;
			ls->first_dir_written = TRUE;
			(ls->multidir) ? ft_putstr(cur->name) : NULL;
			(ls->multidir) ? ft_putendl(":") : NULL;
			ls_write(ls, dir_content, TRUE);
			item_lst_free(&dir_content);
		}
		directories = directories->next;
	}
}

void	ls_directories(t_ls *ls)
{
	t_list	*directories;

	directories = NULL;
	directories = item_group_init(ls, ls->path_dir);
	directories = items_sort(ls, directories);
	ls->first_dir_written = FALSE;
	ls_directories_more(ls, directories);
	item_lst_free(&directories);
}

void	ls_files(t_ls *ls)
{
	t_list	*files;

	files = NULL;
	files = item_group_init(ls, ls->path_files);
	files = items_sort(ls, files);
	if (files)
	{
		ls_write(ls, files, FALSE);
		item_lst_free(&files);
	}
}

void	ls_builder(t_ls *ls)
{
	if (ls->path_files)
		ls_files(ls);
	if (ls->path_files && ls->path_dir)
		ft_putchar('\n');
	if (ls->path_dir)
		ls_directories(ls);
}

void	ls_kernel(t_ls *ls)
{
	DIR		*dir;
	t_list	*path;

	path = ls->path;
	while (path)
	{
		if ((dir = opendir(path->content)) == NULL)
			if (errno != ENOTDIR)
				print_error("ft_ls: ", path->content, FALSE);
			else
				ft_lstpushback(&ls->path_files,
				ft_lstnew(path->content, path->content_size));
		else
		{
			ft_lstpushback(&ls->path_dir,
			ft_lstnew(path->content, path->content_size));
			if (closedir(dir) == -1)
				print_error("ft_ls: ", path->content, FALSE);
		}
		path = path->next;
	}
	ls_builder(ls);
}
