/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 17:51:11 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 22:43:40 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_ls	*ls_init(void)
{
	t_ls	*ls;

	if (!(ls = (t_ls*)malloc(sizeof(t_ls))))
		malloc_error("ls_init => ls");
	if (!(ls->opts = (char*)malloc(sizeof(char) * ft_strlen(ENABLED_OPTS))))
		malloc_error("ls_init => ls->opts");
	ls->path = NULL;
	ls->path_files = NULL;
	ls->path_dir = NULL;
	ls->optset_before = TRUE;
	ls->first_dir_written = FALSE;
	ls->mor_written = FALSE;
	ls->multidir = FALSE;
	return (ls);
}

void	check_arg(t_ls *ls, int argc, char **argv)
{
	int i;

	i = 0;
	while (i++ < argc - 1)
	{
		if (*(argv[i]) == '-' && ls->optset_before)
			check_opt(ls, argv[i]);
		else
			check_path(ls, argv[i]);
	}
}

int		main(int argc, char **argv)
{
	t_ls	*ls;

	ls = ls_init();
	if (argc > 1)
		check_arg(ls, argc, argv);
	if (ls->path == NULL)
		ft_lstpushback(&ls->path, ft_lstnew(".", ft_strlen(".")));
	ls->multidir = (ls->path->next) ? TRUE : FALSE;
	if (enabled_opt(ls, 'f'))
		add_opt(ls, 'a');
	ls_kernel(ls);
	return (0);
}
