/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_items_sort.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/11 19:04:05 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/03 11:21:58 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_list	*sort(t_list *list, int (*sort_cmp)(t_item *i1, t_item *i2))
{
	t_list	*cur;
	t_list	*nxt;
	t_item	*tmp;

	cur = list;
	nxt = list->next;
	while (nxt != NULL)
	{
		while (nxt != cur)
		{
			if (sort_cmp((t_item*)cur->content, (t_item*)nxt->content) > 0)
			{
				tmp = cur->content;
				cur->content = nxt->content;
				nxt->content = tmp;
			}
			cur = cur->next;
		}
		cur = list;
		nxt = nxt->next;
	}
	return (cur);
}

void	items_reverse_sort(t_list **list)
{
	ft_lstrev(list);
}

t_list	*items_sort(t_ls *ls, t_list *items)
{
	t_list	*new;

	if (!items)
		return (NULL);
	new = items;
	if (!enabled_opt(ls, 'f'))
	{
		new = sort(new, cmp_ascii);
		if (enabled_opt(ls, 't'))
			new = sort(new, cmp_date);
		if (enabled_opt(ls, 'r'))
			items_reverse_sort(&new);
	}
	return (new);
}
