/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_item.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/05 14:58:23 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 15:39:00 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_item	*item_init(t_ls *ls, char *name, char *path)
{
	t_item			*item;
	struct stat		stat;

	if (!(item = (t_item*)malloc(sizeof(t_item))))
		malloc_error("item init => item");
	item->name = ft_strdup(name);
	item->path = ft_strjoin(path, name);
	if (lstat(item->path, &stat) == -1)
	{
		print_error("ft_ls: ", item->name, TRUE);
		return (NULL);
	}
	item->st_mode = stat.st_mode;
	item->st_nlink = stat.st_nlink;
	item->st_uid = stat.st_uid;
	item->st_gid = stat.st_gid;
	item->st_size = stat.st_size;
	item->st_blocks = stat.st_blocks;
	item->st_rdev = stat.st_rdev;
	item->date = (enabled_opt(ls, 'u')) ? stat.st_atime : stat.st_mtime;
	ft_strdel(&path);
	return (item);
}

t_list	*item_group_init(t_ls *ls, t_list *item_path)
{
	t_list	*items;
	t_item	*item;

	items = NULL;
	if (item_path)
	{
		while (item_path)
		{
			item = item_init(ls, item_path->content, ft_strdup(""));
			ft_lstpushback(&items, ft_lstnew(item, sizeof(t_item)));
			item_path = item_path->next;
		}
	}
	else
	{
		item = item_init(ls, item_path->content, ft_strdup(""));
		ft_lstpushback(&items, ft_lstnew(item, sizeof(t_item)));
	}
	return (items);
}

t_item	*item_get(t_ls *ls, struct dirent *file, char *path)
{
	t_item	*item;

	if (!file)
		return (NULL);
	item = item_init(ls, file->d_name, ft_strjoin(path, "/"));
	return (item);
}

void	item_free(void *item, size_t size)
{
	if (item && size > 0)
	{
		ft_strdel(&((t_item*)(item))->name);
		ft_strdel(&((t_item*)(item))->path);
		ft_memdel((void**)&item);
	}
}

void	item_lst_free(t_list **lst)
{
	t_list	*tmp;

	tmp = *lst;
	while (tmp)
	{
		*lst = tmp;
		tmp = tmp->next;
		item_free((t_item*)(*lst)->content, sizeof(t_item));
		ft_memdel((void**)lst);
	}
	*lst = NULL;
}
