/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/16 15:43:49 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 22:17:45 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_number(int number, int space)
{
	int		n;

	n = space - ft_strlen_free(ft_itoa(number));
	while (n-- > 0)
		ft_putchar(' ');
	ft_putnbr(number);
	ft_putstr(" ");
}

void	print_number_st_size(t_form *form, int number)
{
	int n;

	n = form->minor + form->major + 2;
	while (n-- > 0)
		ft_putchar(' ');
	print_number(number, form->size);
}

void	print_string_colored(char *str, mode_t mode, t_bool g_enabled)
{
	if (g_enabled)
		get_color(mode);
	ft_putstr(str);
	if (g_enabled)
		ft_putstr("\033[0m");
}

void	print_string(char *str, int space)
{
	int		n;

	n = space - ft_strlen(str);
	ft_putstr(str);
	while (n-- > 0)
		ft_putchar(' ');
	ft_putstr("  ");
}

void	print_convert_time_t(time_t date)
{
	char	*cdate;
	char	*tmp;

	cdate = ctime(&date);
	if ((time(0) - SIX_MONTH_SECOND) > date || time(0) < date)
	{
		tmp = ft_strsub(cdate, 20, 4);
		cdate = ft_strsub(cdate, 4, 6);
		cdate = ft_strmerge(cdate, ft_strdup("  "));
		cdate = ft_strmerge(cdate, tmp);
	}
	else
		cdate = ft_strsub(cdate, 4, 12);
	cdate[12] = '\0';
	ft_putstr(cdate);
	ft_putchar(' ');
	ft_strdel(&cdate);
}
