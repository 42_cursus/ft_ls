/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_print_more.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/03 15:58:39 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/13 10:15:29 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_type(t_item *item)
{
	(S_ISFIFO(item->st_mode) ? ft_putchar('p') : NULL);
	(S_ISDIR(item->st_mode) ? ft_putchar('d') : NULL);
	(S_ISBLK(item->st_mode) ? ft_putchar('b') : NULL);
	(S_ISCHR(item->st_mode) ? ft_putchar('c') : NULL);
	(S_ISREG(item->st_mode) ? ft_putchar('-') : NULL);
	(S_ISLNK(item->st_mode) ? ft_putchar('l') : NULL);
	(S_ISSOCK(item->st_mode) ? ft_putchar('p') : NULL);
	print_access(item);
}

void	print_access(t_item *item)
{
	ft_putchar((item->st_mode & S_IRUSR) ? 'r' : '-');
	ft_putchar((item->st_mode & S_IWUSR) ? 'w' : '-');
	if (item->st_mode & S_ISUID)
		ft_putchar((item->st_mode & S_IXUSR) ? 's' : 'S');
	else
		ft_putchar((item->st_mode & S_IXUSR) ? 'x' : '-');
	ft_putchar((item->st_mode & S_IRGRP) ? 'r' : '-');
	ft_putchar((item->st_mode & S_IWGRP) ? 'w' : '-');
	if (item->st_mode & S_ISGID)
		ft_putchar((item->st_mode & S_IXUSR) ? 's' : 'S');
	else
		ft_putchar((item->st_mode & S_IXGRP) ? 'x' : '-');
	ft_putchar((item->st_mode & S_IROTH) ? 'r' : '-');
	ft_putchar((item->st_mode & S_IWOTH) ? 'w' : '-');
	if (item->st_mode & S_ISVTX)
		ft_putchar((item->st_mode & S_IXUSR) ? 't' : 'T');
	else
		ft_putchar((item->st_mode & S_IXOTH) ? 'x' : '-');
	print_acl(item);
}

void	print_acl(t_item *item)
{
	acl_t	acl;
	int		xattrs;

	xattrs = (int)listxattr(item->path, NULL, 1, XATTR_NOFOLLOW);
	if (xattrs > 0)
		ft_putchar('@');
	else
	{
		acl = acl_get_file(item->path, ACL_TYPE_EXTENDED);
		if (acl != NULL)
			ft_putchar('+');
		else
			ft_putchar(' ');
	}
	ft_putchar(' ');
}

void	print_major_minor(t_item *item, t_form *form)
{
	size_t	minspace;
	size_t	majspace;
	size_t	minor;
	size_t	major;

	minspace = form->minor;
	majspace = form->major;
	minor = ft_strlen_free(ft_itoa(MINOR(item->st_rdev)));
	major = ft_strlen_free(ft_itoa(MAJOR(item->st_rdev)));
	ft_putchar(' ');
	while (major < majspace--)
		ft_putchar(' ');
	ft_putnbr(MAJOR(item->st_rdev));
	ft_putstr(", ");
	while (minor < minspace--)
		ft_putchar(' ');
	ft_putnbr(MINOR(item->st_rdev));
	ft_putchar(' ');
}

void	print_ln(t_item *item, t_bool link)
{
	char	path_link[1024];
	int		size;

	if (!link)
		return ;
	if ((size = readlink(item->path, path_link, 1024)) > 0)
	{
		ft_putstr(" -> ");
		path_link[size] = '\0';
		ft_putstr(path_link);
	}
}
