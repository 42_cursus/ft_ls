/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/14 17:38:21 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/13 10:15:54 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	check_path(t_ls *ls, char *path)
{
	if (!ft_strcmp(path, """") || !ft_strcmp(path, "''"))
	{
		ft_putendl_fd("ft_ls: fts_open: No such file or directory", 2);
		exit(EXIT_FAILURE);
	}
	ls->optset_before = FALSE;
	ft_lstpushback(&(ls->path), ft_lstnew(path, ft_strlen(path)));
}

t_bool	cmp_date(t_item *item1, t_item *item2)
{
	if (item1->date == item2->date)
		return (-1 * ft_strcmp(item2->name, item1->name));
	return (item1->date < item2->date);
}

t_bool	cmp_ascii(t_item *item1, t_item *item2)
{
	return (ft_strcmp(item1->name, item2->name));
}

void	get_color(mode_t mode)
{
	if (S_ISBLK(mode))
		ft_putstr("\033[31m");
	if (S_ISCHR(mode))
		ft_putstr("\033[34m");
	if (S_ISDIR(mode))
		ft_putstr("\033[36m");
	if (S_ISFIFO(mode))
		ft_putstr("\033[33m");
	if (S_ISREG(mode))
		ft_putstr("\033[0m");
	if (S_ISLNK(mode))
		ft_putstr("\033[32m");
	if (S_ISSOCK(mode))
		ft_putstr("\033[35m");
}

char	*get_absolute_path(char *path)
{
	char actualpath[4097];
	char *ptr;

	ptr = realpath(path, actualpath);
	return (ptr);
}
