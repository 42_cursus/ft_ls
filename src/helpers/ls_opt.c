/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_opt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 15:25:53 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/03 17:54:41 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_bool	is_opt(char opt)
{
	if (ft_strchr(ENABLED_OPTS, opt))
		return (TRUE);
	return (FALSE);
}

void	add_opt(t_ls *ls, char opt)
{
	static size_t i = 0;

	if (enabled_opt(ls, opt) == FALSE)
		ls->opts[i++] = opt;
}

t_bool	enabled_opt(t_ls *ls, char opt)
{
	if (ft_strchr(ls->opts, opt))
		return (TRUE);
	return (FALSE);
}

void	check_opt(t_ls *ls, char *opt)
{
	if (ft_strcmp(opt, "--") == 0 || ft_strcmp(opt, "-1") == 0)
		return ;
	else
		while (*opt++)
		{
			if (is_opt(*opt) == TRUE)
				add_opt(ls, *opt);
			else
				illegal_usage_error(*opt);
		}
}
