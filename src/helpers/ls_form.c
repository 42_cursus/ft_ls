/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_form.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/06 13:24:52 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/08 17:07:51 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	form_get_correct(t_form *form, t_item *cur)
{
	if (getpwuid(cur->st_uid))
		form->user = ft_strlen(getpwuid(cur->st_uid)->pw_name) \
			> form->user ? ft_strlen(getpwuid(cur->st_uid)->pw_name) \
			: form->user;
	else
		form->user = ft_strlen_free(ft_itoa(cur->st_uid)) \
			> form->user ? ft_strlen_free(ft_itoa(cur->st_uid)) \
			: form->user;
	if (getgrgid(cur->st_gid))
		form->group = ft_strlen(getgrgid(cur->st_gid)->gr_name) \
			> form->group ? \
			ft_strlen(getgrgid(cur->st_gid)->gr_name) : form->group;
	else
		form->group = ft_strlen_free(ft_itoa(cur->st_gid)) \
			> form->group ? ft_strlen_free(ft_itoa(cur->st_gid)) \
			: form->group;
}

void	form_get(t_form *form, t_item *cur)
{
	form->link = ft_strlen_free(ft_itoa(cur->st_nlink)) > \
		form->link ? ft_strlen_free(ft_itoa(cur->st_nlink)) \
		: form->link;
	form->major = ft_strlen_free(ft_itoa(MAJOR(cur->st_rdev))) > form->major \
		? ft_strlen_free(ft_itoa(MAJOR(cur->st_rdev))) : form->major;
	form->minor = ft_strlen_free(ft_itoa(MINOR(cur->st_rdev))) > form->minor ? \
		ft_strlen_free(ft_itoa(MINOR(cur->st_rdev))) : form->minor;
	form->size = ft_strlen_free(ft_itoa(cur->st_size)) > form->size ? \
		ft_strlen_free(ft_itoa(cur->st_size)) : form->size;
	form->total += cur->st_blocks;
}

t_form	*form_init(t_ls *ls, t_list *items)
{
	t_form	*form;
	t_item	*tmp;

	if (!(form = (t_form*)malloc(sizeof(t_form))))
		malloc_error("form_init => form");
	form->total = 0;
	form->size = 0;
	form->group = 0;
	form->user = 0;
	form->link = 0;
	form->minor = 0;
	form->major = 0;
	while (items)
	{
		tmp = (t_item*)items->content;
		if (!(enabled_opt(ls, 'a') == FALSE && tmp->name[0] == '.'))
		{
			form_get(form, tmp);
			form_get_correct(form, tmp);
		}
		items = items->next;
	}
	tmp = NULL;
	return (form);
}
