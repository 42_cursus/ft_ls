/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 16:31:10 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/03 11:37:28 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	malloc_error(char *code)
{
	ft_putstr_fd("MALLOC ERROR : ", 2);
	ft_putendl_fd(code, 2);
	exit(EXIT_FAILURE);
}

void	illegal_usage_error(char opt)
{
	ft_putstr_fd(ILLEGAL_ERROR, 2);
	ft_putchar_fd(opt, 2);
	ft_putchar_fd('\n', 2);
	ft_putendl_fd(USAGE_ERROR, 2);
	exit(EXIT_FAILURE);
}

void	print_error(char *name, char *error, t_bool ext)
{
	ft_putstr_fd(name, 2);
	perror(error);
	if (ext)
		exit(EXIT_FAILURE);
}
