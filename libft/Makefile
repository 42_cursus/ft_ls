# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/04 16:56:16 by adelhom           #+#    #+#              #
#    Updated: 2017/06/13 00:48:05 by adelhom          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a

CC = gcc
FLAGS = -Wall -Wextra -Werror

PATH_SRC	= ./src
PATH_OBJ	= ./obj
PATH_INC	= ./includes

SRC_FILES	:= $(shell find $(PATH_SRC) -type f | grep -E "\.c$$")
SRC_DIRS_RAW:= $(shell find $(PATH_SRC) -type d -mindepth 1)
SRC_DIRS = $(SRC_DIRS_RAW:./src/%=%)

SRC		= $(SRC_FILES:./src/%=%)
OBJ		= $(addprefix $(PATH_OBJ)/,$(SRC:.c=.o))
INC		= $(addprefix -I,$(PATH_INC))

all: $(PATH_OBJ) $(NAME)

$(PATH_OBJ):
	mkdir -p $(PATH_OBJ)
	mkdir -p $(addprefix $(PATH_OBJ)/,$(SRC_DIRS))

$(PATH_OBJ)/%.o:$(PATH_SRC)/%.c
	$(CC) $(FLAGS) $(INC) -o $@ -c $^
	echo "\033[0;32m[✔] \033[1;33m $^ \033[0m"

$(NAME) : $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)
	echo "\033[1;34mLibft\t\t\033[1;33mcompiled\t\033[0;32m[OK]\033[0m"

.SILENT:
.PHONY: clean fclean re all

clean:
	rm -rf $(PATH_OBJ)
	echo "\033[1;34mLibft\t\t\033[1;33mobjects clean\t\033[0;32m[OK]\033[0m"

fclean: clean
	rm -f $(NAME)
	echo "\033[1;34mLibft\t\t\033[1;33mfiles clean\t\033[0;32m[OK]\033[0m"

re: fclean all
