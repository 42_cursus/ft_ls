/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_point3dnew.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 11:17:53 by adelhom           #+#    #+#             */
/*   Updated: 2017/02/25 18:56:08 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_point3d	*ft_point3dnew(double x, double y, double z)
{
	t_point3d	*point;

	if (!(point = (t_point3d *)malloc(sizeof(t_point3d))))
		return (NULL);
	point->x = x;
	point->y = y;
	point->z = z;
	return (point);
}
