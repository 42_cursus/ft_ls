/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/09 10:59:53 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/13 10:19:26 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_arrccpy(char **arr)
{
	char	**ret;
	size_t	i;

	if ((ret = (char**)malloc(sizeof(char*) * (ft_arrlen(arr) + 1))) == NULL)
		return (NULL);
	i = 0;
	while (arr[i])
	{
		ret[i] = ft_strdup(arr[i]);
		++i;
	}
	ret[i] = NULL;
	return (ret);
}
