/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/04 17:16:36 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/12 15:42:17 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <strings.h>
# include <string.h>
# include <fcntl.h>
# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/uio.h>

# include "libft_array.h"
# include "libft_check.h"
# include "libft_convert.h"
# include "libft_gnl.h"
# include "libft_list.h"
# include "libft_maths.h"
# include "libft_memory.h"
# include "libft_put.h"
# include "libft_str.h"
# include "libft_swap.h"

# define FALSE 0
# define TRUE 1

typedef int	t_bool;

#endif
