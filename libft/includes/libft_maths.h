/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_maths.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/09 11:29:18 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/13 10:27:29 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_MATHS_H
# define LIBFT_MATHS_H

typedef struct	s_point2d
{
	double		x;
	double		y;
}				t_point2d;

typedef struct	s_point3d
{
	double		x;
	double		y;
	double		z;
}				t_point3d;

t_point2d		*ft_point2dnew(double x, double y);
t_point3d		*ft_point3dnew(double x, double y, double z);
size_t			ft_powerint(size_t nb, size_t power);
size_t			ft_sqrtint(size_t nb);

#endif
