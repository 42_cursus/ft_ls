/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_swap.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/09 11:32:14 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/13 10:27:43 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_SWAP_H
# define LIBFT_SWAP_H

void			ft_swapdouble(double *a, double *b);
void			ft_swapint(int *a, int *b);

#endif
