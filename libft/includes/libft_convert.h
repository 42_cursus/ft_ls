/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_convert.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/09 11:32:23 by adelhom           #+#    #+#             */
/*   Updated: 2017/06/13 10:26:48 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_CONVERT_H
# define LIBFT_CONVERT_H

int				ft_toupper(int c);
int				ft_tolower(int c);
char			*ft_itoa(int n);
int				ft_atoi(char *str);

#endif
