# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: adelhom <adelhom@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/16 12:46:43 by adelhom           #+#    #+#              #
#    Updated: 2017/05/25 15:13:27 by adelhom          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls

CC = gcc
FLAGS = -Wall -Wextra -Werror

PATH_SRC	= ./src
PATH_OBJ	= ./obj
PATH_INC	= ./includes $(LIBFT_INC)

SRC_FILES		:= $(shell find $(PATH_SRC) -type f | grep -E "\.c$$")
SRC_DIRS_RAW	:= $(shell find $(PATH_SRC) -type d -mindepth 1)
SRC_DIRS 		= $(SRC_DIRS_RAW:./src/%=%)

# LIBFT library
LIBFT		= ./libft/
LIBFT_INC	= $(LIBFT)/includes
LIBFT_LIB	= $(addprefix $(LIBFT), libft.a)
LIBFT_FLAGS	= -L$(LIBFT) -lft

SRC		= $(SRC_FILES:./src/%=%)
OBJ		= $(addprefix $(PATH_OBJ)/, $(SRC:.c=.o))
INC		= $(addprefix -I, $(PATH_INC))

all: $(PATH_OBJ) $(NAME)

$(PATH_OBJ):
	mkdir -p $(PATH_OBJ)
	mkdir -p $(addprefix $(PATH_OBJ)/, $(SRC_DIRS))

$(PATH_OBJ)/%.o: $(PATH_SRC)/%.c
	$(CC) $(FLAGS) $(INC) -o $@ -c $< 
	echo "\033[0;32m[✔] \033[1;33m $< \033[0m"

$(NAME): $(LIBFT_LIB) $(OBJ)
	$(CC) $(LIBFT_FLAGS) -o $(NAME) $(OBJ)
	echo "\033[1;34mft_ls\t\t\033[1;33mcompiled\t\033[0;32m[OK]\033[0m"

$(LIBFT_LIB):
	make -C $(LIBFT)

.SILENT:
.PHONY: clean fclean re all build

clean:
	make -C $(LIBFT) clean
	rm -rf $(PATH_OBJ)
	echo "\033[1;34mft_ls\t\t\033[1;33mobjects clean\t\033[0;32m[OK]\033[0m"

fclean: clean
	make -C $(LIBFT) fclean
	rm -f $(NAME)
	echo "\033[1;34mft_ls\t\t\033[1;33mfiles clean\t\033[0;32m[OK]\033[0m"

re: fclean all

dev : all
	./$(NAME)
